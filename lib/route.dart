/*
module  : ROUTE
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : HIPMINET
version : v1.0
*/
part of 'app.dart';
Map<String, Widget Function(BuildContext)> routes = {
	SplashScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SplashController(), child: SplashScreen()),
	SigninScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: SigninController(), child: SigninScreen()),
	ForgotpasswordScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: ForgotpasswordController(), child: ForgotpasswordScreen()),
	DashboardScreen.routeName: (ctx) => ChangeNotifierProvider.value(value: DashboardController(), child: DashboardScreen()),
};
