/*
module  : STYLING
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : HIPMINET
version : v1.0
*/

part of 'app.dart';

double defaultMargin = 25;

Color primaryColor = HexColor.fromHex("#191923");
Color secondaryColor = HexColor.fromHex("#FF5A00");
Color lightColor = HexColor.fromHex("#FFFFFF");
Color grayColor = HexColor.fromHex("#979DA7");
Color lightGrayColor = grayColor.withOpacity(0.5);

Color successColor = HexColor.fromHex("#2ecc71");
Color warningColor = HexColor.fromHex("#f39c12");
Color errorColor = HexColor.fromHex("#e74c3c");

Gradient gradientColor69 = LinearGradient(
  colors: [
    HexColor.fromHex("#FB6341"),
    HexColor.fromHex("#FB9840"),
  ],
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
);

class RectangleClipPath extends CustomClipper<Path> {
  var radius = 10.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, 0.0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

Color warnigColor69 = HexColor.fromHex("#ffb002");
Color lightGrayColor69 = HexColor.fromHex("#868686").withOpacity(0.3);

TextStyle fontMuli = GoogleFonts.muli(fontStyle: FontStyle.normal);
TextStyle fontNonito = GoogleFonts.nunito(fontStyle: FontStyle.normal);

TextStyle textLight =
    fontMuli.copyWith(color: lightColor, fontWeight: FontWeight.w500);
TextStyle textPrimary =
    fontMuli.copyWith(color: primaryColor, fontWeight: FontWeight.w500);
TextStyle textSecondary =
    fontMuli.copyWith(color: secondaryColor, fontWeight: FontWeight.w500);

TextStyle textLightNunito =
    fontNonito.copyWith(color: lightColor, fontWeight: FontWeight.w500);
TextStyle textPrimaryNunito =
    fontNonito.copyWith(color: primaryColor, fontWeight: FontWeight.w500);
TextStyle textSecondaryNunito =
    fontNonito.copyWith(color: secondaryColor, fontWeight: FontWeight.w500);

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double defaultSize;
  static Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenHeight = _mediaQueryData.size.height;
    screenWidth = _mediaQueryData.size.width;
    orientation = _mediaQueryData.orientation;

    defaultSize = orientation == Orientation.landscape
        ? screenHeight * 0.024
        : screenWidth * 0.024;
  }
}
