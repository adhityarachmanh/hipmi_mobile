/*
module  : DASHBOARD CONTROLLER
creator : arh
os      : linux-gnu
created : Sel 22 Des 2020 11:31:02  WIB
product : HIPMINET
version : v1.0
*/

part of '../../app.dart';

class DashboardController with ChangeNotifier {
  final String title = "DashboardControllerWorks!";

  List<Map<String, dynamic>> itemDrawer = [
    {"title": "HIPMI", "icon": "hipmi"},
    {"title": "e-KTA", "icon": "kta"},
    {"title": "Event", "icon": "event"},
    {"title": "Pengajuan", "icon": "pengajuan"},
    {"title": "Toko", "icon": "toko"},
    {"title": "Tentang", "icon": "tentang"}
  ];

  void onInit(BuildContext context) {
    print("Init DashboardController");
  }
}
