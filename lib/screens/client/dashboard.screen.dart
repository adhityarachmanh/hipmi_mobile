/*
module  : DASHBOARD SCREEN
creator : arh
os      : linux-gnu
created : Sel 22 Des 2020 11:31:02  WIB
product : HIPMINET
version : v1.0
*/

part of '../../app.dart';

class DashboardScreen extends StatefulWidget {
  static final routeName = "/DashboardScreen";

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<DashboardController>(context);
    final dispatch = Provider.of<DashboardController>(context, listen: false);
    // Init controller
    // dispatch.onInit(context);

    return Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(gradient: gradientColor69),
              ),
              SafeArea(
                child: Container(
                  decoration: BoxDecoration(gradient: gradientColor69),
                ),
              ),
              ListView(
                children: [
                  Container(
                    padding: EdgeInsets.all(defaultMargin),
                    child: Row(
                      children: [
                        Container(
                          height: 72,
                          width: 72,
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: lightColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Image.asset("assets/images/hipmi2.png"),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Himpunan Pengusaha",
                                style: textLightNunito.copyWith(
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "Muda Indonesia",
                                style: textLightNunito.copyWith(
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: defaultMargin),
                    child: Column(
                      children: state.itemDrawer
                          .map((e) => Container(
                                margin: EdgeInsets.only(bottom: defaultMargin),
                                padding: EdgeInsets.symmetric(
                                    horizontal: defaultMargin),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                        "assets/icons/drawer/${e['icon']}.svg"),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Text(
                                      e['title'],
                                      style: textLightNunito.copyWith(
                                          fontSize: 16),
                                    )
                                  ],
                                ),
                              ))
                          .toList(),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(color: lightColor),
            ),
            SafeArea(
              child: Container(
                  decoration:
                      BoxDecoration(color:lightColor)),
            ),
            ListView(
              children: [
                // SliverAppBar(leading: Icon(Icons.ac_unit),)
                Container(
                  height: SizeConfig.screenHeight * 0.34,
                  // margin: EdgeInsets.only(top: defaultMargin),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 0,
                        left: 0,
                        child: Image.asset(
                          "assets/images/test-image.jpg",
                          height: SizeConfig.screenHeight * 0.34,
                        ),
                      ),
                      Positioned(
                        bottom: -15,
                        left: 0,
                        child: Container(
                          width: SizeConfig.screenWidth,
                          // height: 90,
                          child: Image.asset(
                            "assets/images/rectangle117.png",
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 70,
                        left: 0,
                        child: Container(
                          // height: 50,

                          child: Image.asset(
                            "assets/images/substract_gradient.png",
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 45,
                        left: 0,
                        child: Container(
                          // height: 50,
                          child: Image.asset(
                            "assets/images/substract_white.png",
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: -45,
                        left: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              color: lightColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(50))),
                          width: SizeConfig.screenWidth,
                          margin: EdgeInsets.only(right: 40),
                          height: 90,
                        ),
                      ),
                      GestureDetector(
                        onTap: () => _scaffoldKey.currentState.openDrawer(),
                        child: ClipPath(
                          clipper: RectangleClipPath(),
                          child: Container(
                            height: SizeConfig.screenHeight * 0.08,
                            decoration: BoxDecoration(color: lightColor),
                            padding: EdgeInsets.symmetric(
                                horizontal: defaultMargin * 0.7),
                            child: Row(
                              children: [
                                Container(
                                  height: 24,
                                  width: 24,
                                  child: SvgPicture.asset(
                                    "assets/icons/toggle.svg",
                                    color: warnigColor69,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  // height: SizeConfig.screenHeight * 0.5,
                  // margin: EdgeInsets.only(right:defaultMargin*0.5),
                  decoration: BoxDecoration(color: lightColor),
                )
              ],
            )
          ],
        ));
  }
}
