/*
module  : SIGNIN SCREEN
creator : arh
os      : linux-gnu
created : Jum 18 Des 2020 02:10:09  WIB
product : HIPMINET
version : v1.0
*/

part of '../../../app.dart';

class SigninScreen extends StatelessWidget {
  static final routeName = "/SigninScreen";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SigninController>(context);
    final dispatch = Provider.of<SigninController>(context, listen: false);
    // Init controller
    // dispatch.onInit(context);

    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(color: secondaryColor),
        ),
        SafeArea(
          child: Container(
            decoration: BoxDecoration(color: lightColor),
          ),
        ),
        Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                width: SizeConfig.screenWidth,
                child: Image.asset("assets/images/up.png"),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: Container(
                width: SizeConfig.screenWidth,
                child: Image.asset("assets/images/down.png"),
              ),
            ),
          ],
        ),
        Scaffold(
            backgroundColor: Colors.transparent,
            body: ListView(
              children: [
                Container(
                  margin: EdgeInsets.only(top: defaultMargin * 3),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        // width: SizeConfig.screenWidth * 0.2,
                        width: 90,
                        child: Image.asset("assets/images/signin-1.png"),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: defaultMargin * 0.3),
                        width: 200,
                        // width: SizeConfig.screenWidth * 0.5,
                        child: SvgPicture.asset(
                          "assets/images/hipmi1.svg",
                          color: HexColor.fromHex("#d82727"),
                        ),
                      ),
                      Container(
                        width: SizeConfig.screenWidth * 0.2,
                        margin: EdgeInsets.only(
                          top: defaultMargin * 0.75,
                        ),
                        child: SvgPicture.asset(
                          "assets/images/people.svg",
                        ),
                      ),
                      Container(
                          width: SizeConfig.screenWidth * 0.8,
                          margin: EdgeInsets.only(
                            top: defaultMargin * 0.5,
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 0, horizontal: defaultMargin),
                          decoration: BoxDecoration(
                            color: lightGrayColor69,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: TextField(
                            style: textPrimary.copyWith(
                                fontSize: 14, color: primaryColor),
                            // controller: usernameController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: "Email atau username",
                              hintStyle: textLight.copyWith(color: lightColor),
                              border: InputBorder.none,
                            ),
                          )),
                      Container(
                          width: SizeConfig.screenWidth * 0.8,
                          margin: EdgeInsets.only(top: defaultMargin * 0.5),
                          padding: EdgeInsets.symmetric(
                              vertical: 0, horizontal: defaultMargin),
                          decoration: BoxDecoration(
                            color: lightGrayColor69,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: TextField(
                            style: textPrimary.copyWith(
                                fontSize: 14, color: primaryColor),
                            // controller: usernameController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              suffix: Container(
                                width: 5,
                                child: SvgPicture.asset(
                                  "assets/icons/pass-on.svg",
                                ),
                              ),
                              // suffix:,
                              hintText: "Password",
                              hintStyle: textLight.copyWith(color: lightColor),
                              border: InputBorder.none,
                            ),
                          )),
                      Container(
                        margin: EdgeInsets.only(top: defaultMargin * 0.8),
                        width: SizeConfig.screenWidth * 0.5,
                        height: 40,
                        padding: EdgeInsets.symmetric(
                            vertical: 0, horizontal: defaultMargin),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: RaisedButton(
                            color: warnigColor69,
                            child: Text("Log In",
                                style: textPrimary.copyWith(fontSize: 18)),
                            onPressed: () =>route.navigateToAndRemoveUntil(DashboardScreen.routeName),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: defaultMargin * 1.2),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                          color: primaryColor,
                          width: 1, // Underline thickness
                        ))),
                        child: GestureDetector(
                          onTap: () =>
                              route.navigateTo(ForgotpasswordScreen.routeName),
                          child: Text("Lupa password?",
                              style: textPrimary.copyWith(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              )),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: defaultMargin * 1.5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: SizeConfig.screenWidth * 0.35,
                              height: 1,
                              decoration: BoxDecoration(color: secondaryColor),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: defaultMargin,
                              ),
                              child: Text(
                                "ATAU",
                                style: textSecondary.copyWith(
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                            Container(
                              width: SizeConfig.screenWidth * 0.35,
                              height: 1,
                              decoration: BoxDecoration(color: secondaryColor),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: defaultMargin * 1.5),
                        child: Column(
                          children: [
                            Text("Belum memiliki akun?"),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                color: primaryColor,
                                width: 1, // Underline thickness
                              ))),
                              child: Text("Daftar disini.",
                                  style: textPrimary.copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )),
      ],
    );
  }
}
