/*
module  : FORGOTPASSWORD SCREEN
creator : arh
os      : linux-gnu
created : Sen 21 Des 2020 05:10:56  WIB
product : HIPMINET
version : v1.0
*/

part of '../../../app.dart';

class ForgotpasswordScreen extends StatelessWidget {
  static final routeName = "/ForgotpasswordScreen";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<ForgotpasswordController>(context);
    final dispatch = Provider.of<ForgotpasswordController>(context, listen: false);
    // Init controller
    // dispatch.onInit(context);
    
    return Scaffold(
      body: Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeight,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            Text("ForgotpasswordScreenWorks!",style: textPrimary.copyWith(fontSize:25)),
            SizedBox(height:defaultMargin),
            Text(state.title,style: textPrimary.copyWith(fontSize:20)),
          ],)
        ),
      ),
    );
  }
}
