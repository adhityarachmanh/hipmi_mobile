/*
module  : SPLASH SCREEN
creator : adhityarachmanh
os      : darwin19
created : Thu Dec 17 20:21:01 WIB 2020
product : HIPMINET
version : v1.0
*/

part of '../../app.dart';

class SplashScreen extends StatelessWidget {
  static final routeName = "/SplashScreen";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Global controller
    final globalState = Provider.of<IndexController>(context);
    final globalDispatch = Provider.of<IndexController>(context, listen: false);
    // Route
    final route = Provider.of<RouteFunction>(context, listen: false);
    final routeParams = route.getParams(context);
    // Websocket
    final websocket = Provider.of<WebsocketFunction>(context, listen: false);
    // Local controller
    final state = Provider.of<SplashController>(context);
    final dispatch = Provider.of<SplashController>(context, listen: false);
    // Init controller
    dispatch.onInit(context); 
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: secondaryColor),
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeight,
        child: Image.asset("assets/images/splash.png"),
      ),
    );
  }
}
